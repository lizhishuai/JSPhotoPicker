//
//  JSPhotoPickerViewController.m
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import "JSPhotoPickerViewController.h"
#import "JSPhotoGroupViewController.h"
#import "JSPhotoPickerNavigationController.h"
#import "JSPhotoPickerCommon.h"
#import <Photos/Photos.h>
#import "JSPhotoPickerDatas.h"
@interface JSPhotoPickerViewController ()
@property (nonatomic , weak) JSPhotoGroupViewController *groupVC;
@end

@implementation JSPhotoPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addNotification];
    // Do any additional setup after loading the view.
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)addNotification{
    // 监听异步done通知
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(done:) name:PICKER_TAKE_DONE object:nil];
    });
}
- (void) done:(NSNotification *)note{
    NSMutableArray *selectArray =  [note.userInfo[@"selectAssets"] mutableCopy];
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(pickerViewControllerDoneAsstes:)]) {
            [self.delegate pickerViewControllerDoneAsstes:selectArray];
        }else if (self.callBack){
            NSMutableArray *imageArray = [@[] mutableCopy];
            for (PHAsset *asset in selectArray) {
                [[JSPhotoPickerDatas defaultPicker] getAssetsImageWithAsset:asset targetSize:CGSizeZero sys:YES andFinish:^(id objc) {
                    [imageArray addObject:objc];
                }];
            }
//            NSLog(@"%@",selectArray);
//            if (self.callBack) {
                self.callBack(selectArray, imageArray);
//            }
//            self.callBack(selectArray);
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
         [self createNavController];
    }
    return self;
}
-(void)createNavController{
    JSPhotoGroupViewController *groupVC = [[JSPhotoGroupViewController alloc] init];
    JSPhotoPickerNavigationController *nav = [[JSPhotoPickerNavigationController alloc] initWithRootViewController:groupVC];
    nav.view.frame = self.view.bounds;
    [self addChildViewController:nav];
    [self.view addSubview:nav.view];
    self.groupVC = groupVC;
}
-(void)showPickerViewController:(UIViewController *)VC{
    __weak typeof(VC)weakVC = VC;
    if (weakVC != nil) {
        [weakVC presentViewController:self animated:YES completion:nil];
    }
}
-(void)setMaxCount:(NSInteger)maxCount{
    if (maxCount <= 0) return;
    _maxCount = maxCount;
    self.groupVC.maxCount = maxCount;
}
-(void)setStatus:(PickerViewShowStatus)status{
    _status = status;
    self.groupVC.status = status;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
