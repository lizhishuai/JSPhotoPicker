//
//  JSPhotoPickerViewController.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
// 回调
typedef void(^callBackBlock)(NSMutableArray <PHAsset *>* assetArray, NSMutableArray <UIImage *>* imageArray);

typedef NS_ENUM(NSInteger){
    PickerViewShowStatusCameraRoll,
    PickerViewShowStatusVideo
}PickerViewShowStatus;
@protocol JSPhotoPickerViewControllerDelegate <NSObject>
/**
 *  返回所有的Asstes对象
 */
- (void) pickerViewControllerDoneAsstes:(NSArray *)selectedAssets;
@end
@interface JSPhotoPickerViewController : UIViewController
@property(nonatomic,assign)PickerViewShowStatus status;
@property(nonatomic,assign)NSInteger maxCount;
@property (nonatomic , weak) id<JSPhotoPickerViewControllerDelegate> delegate;
@property  (nonatomic,copy) callBackBlock callBack;
-(void)showPickerViewController:(UIViewController *)VC;
@end
