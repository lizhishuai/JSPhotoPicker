//
//  JSPhotoGroupViewController.m
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import "JSPhotoGroupViewController.h"
#import "JSPhotoGroupCell.h"
#import "JSPhotoPickerDatas.h"
#import "JSPhotosGroup.h"
#import "JSPhotoPickerAssetSelectViewController.h"
@interface JSPhotoGroupViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSArray *groups;
@end

@implementation JSPhotoGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    [self createCloseBtn];
    typeof(self)weakself = self;
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (status == PHAuthorizationStatusAuthorized) {
            [[JSPhotoPickerDatas defaultPicker] getAllGroupWithPhotos:^(id objc) {
                weakself.groups = objc;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakself.tableView reloadData];
                });
            }];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请先打开访问图库权限" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            [alert show];
        }
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.groups.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JSPhotoGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([JSPhotoGroupCell class]) forIndexPath:indexPath];
    JSPhotosGroup *group ;
    if (self.groups.count > indexPath.row) {
        group = self.groups[indexPath.row];
    }
    cell.group = group;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    JSPhotoPickerAssetSelectViewController *assetVC = [JSPhotoPickerAssetSelectViewController new];
    JSPhotosGroup *group ;
    if (self.groups.count > indexPath.row) {
        group = self.groups[indexPath.row];
    }
    assetVC.maxCount = self.maxCount;
    assetVC.status = self.status;
    assetVC.group = group;
    [self.navigationController pushViewController:assetVC animated:YES];
}
-(void)createCloseBtn{
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"关闭" style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    [item setTintColor:UIColor.blackColor];
    self.navigationItem.rightBarButtonItem = item;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,kSCREEN_WIDTH, kSCREEN_HEIGHT) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[JSPhotoGroupCell class] forCellReuseIdentifier: NSStringFromClass([JSPhotoGroupCell class])];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _tableView;
}
-(void)back{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
