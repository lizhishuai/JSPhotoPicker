//
//  JSPhotoGroupViewController.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSPhotoPickerViewController.h"

#define kSCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define kSCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
@interface JSPhotoGroupViewController : UIViewController
@property(nonatomic,assign)NSInteger maxCount;
@property (nonatomic, assign) PickerViewShowStatus status;
@end
