//
//  JSPhotoBrowsViewController.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/14.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
@interface JSPhotoBrowsViewController : UIViewController
@property(nonatomic,strong)NSMutableArray <PHAsset *>*assetArray;
/**
 *当前图片的index
 */
@property(nonatomic, assign)NSInteger currentIndex;
@property(nonatomic, strong)NSMutableArray *selectedAssetArray;
@property(nonatomic, assign)NSInteger maxCount;
@end
