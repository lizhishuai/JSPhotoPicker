//
//  JSPhotoPickerAssetSelectViewController.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/13.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSPhotoPickerViewController.h"
#import "JSPhotosGroup.h"
@interface JSPhotoPickerAssetSelectViewController : UIViewController
@property(nonatomic,assign)NSInteger maxCount;
@property (nonatomic, assign) PickerViewShowStatus status;
@property(nonatomic, strong)JSPhotosGroup *group;
@end
