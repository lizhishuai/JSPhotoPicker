//
//  JSPhotoPickerCommon.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/13.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#ifndef JSPhotoPickerCommon_h
#define JSPhotoPickerCommon_h

// 图片路径
#define JSPhotoPickerSrcName(file) [@"JSPhotoPicker.bundle" stringByAppendingPathComponent:file]
static NSString *PICKER_TAKE_DONE = @"PICKER_TAKE_DONE";
#endif /* JSPhotoPickerCommon_h */
