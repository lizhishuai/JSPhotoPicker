//
//  JSPhotoPickerDatas.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSPhotosGroup.h"
@interface JSPhotoPickerDatas : NSObject
+(instancetype)defaultPicker;
/**
 *获取所有组
 */
-(void)getAllGroupWithPhotos:(void(^)(id objc))callBack;
-(void)getAllGroupWIthVideos:(void(^)(id objc))callBack;

/**
 *获取一个组里边的Asset
 */
-(void)getAllPhotosWithGroup:(JSPhotosGroup *)group andPhotos:(BOOL)isPhotos andFinished:(void(^)(id objc))callBack;
/**
 *通过PHAsset获取image
 */
-(void)getAssetsImageWithAsset:(PHAsset *)asset targetSize:(CGSize )size sys:(BOOL)synchronous andFinish:(void(^)(id objc))callBack;
@end
