//
//  JSPhotoPickerDatas.m
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import "JSPhotoPickerDatas.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
@implementation JSPhotoPickerDatas
+(instancetype)defaultPicker{
    return [[self alloc] init];
}

/**
 *获取所有组
 */
-(void)getAllGroupWithPhotos:(void(^)(id objc))callBack{
    [self getAllGroupAllPhotos:YES withCallBack:callBack];
}
-(void)getAllGroupWIthVideos:(void(^)(id objc))callBack{
    [self getAllGroupAllPhotos:NO withCallBack:callBack];
}
-(void)getAllGroupAllPhotos:(BOOL)allPhotos withCallBack:(void(^)(id objc))callBack{
    NSMutableArray *groupArray = [NSMutableArray arrayWithCapacity:0];
    // 获取相机胶卷相册
    PHAssetCollection *cameraRoll = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:nil].lastObject;
    NSLog(@"======%@",cameraRoll.localizedTitle);
    if (cameraRoll) {
        JSPhotosGroup *group = [[JSPhotosGroup alloc] init];
        group.assetCollection = cameraRoll;
        group.groupName = cameraRoll.localizedTitle;
        PHFetchResult <PHAsset *> * result = [self getallPhotoWithPHAssetCollection:cameraRoll withPhotos:allPhotos];
        group.assetsCount = result.count;
        [self getAssetsImageWithAsset:[result firstObject] targetSize:CGSizeMake(90, 90) sys:YES andFinish:^(id objc) {
            group.thumbImage = objc;
        }];
        [groupArray addObject:group];
    }
    //自定义相册
    PHFetchResult *result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options: [PHFetchOptions new]];
    [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[PHAssetCollection class]]) {
            PHAssetCollection *asset = (PHAssetCollection *)obj;
            JSPhotosGroup *group = [[JSPhotosGroup alloc] init];
            group.assetCollection = asset;
            group.groupName = asset.localizedTitle;
            PHFetchResult <PHAsset *> * result = [self getallPhotoWithPHAssetCollection:asset withPhotos:allPhotos];
            group.assetsCount = result.count;
            [self getAssetsImageWithAsset:[result firstObject] targetSize:CGSizeMake(100, 100) sys:YES andFinish:^(id objc) {
                group.thumbImage = objc;
            }];
            [groupArray addObject:group];
            NSLog(@"======%@",asset.localizedTitle);
            NSLog(@">>>>>>>>>:%lu",(unsigned long)idx);
        }
    }];
  
    if (callBack) {
        callBack(groupArray);
    }
}
-(PHFetchResult<PHAsset *> *)getallPhotoWithPHAssetCollection:(PHAssetCollection *)assetCollection withPhotos:(BOOL)isPhoto{
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d",isPhoto ? PHAssetMediaTypeImage : PHAssetMediaTypeVideo];
    PHFetchResult<PHAsset *> *assetResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:fetchOptions];
    //    assetResult.count
    NSLog(@"=====%lu",(unsigned long)assetResult.count);
    return assetResult;
    
    
}
/**
 *通过PHAsset获取image
 */
-(void)getAssetsImageWithAsset:(PHAsset *)asset targetSize:(CGSize )size sys:(BOOL)synchronous andFinish:(void(^)(id objc))callBack{
    PHImageRequestOptions *opt = [[PHImageRequestOptions alloc] init];
    opt.synchronous = synchronous;
    opt.networkAccessAllowed = YES;
    PHImageManager *manager = [[PHImageManager alloc] init];
    
    [manager requestImageForAsset:asset targetSize:(!CGSizeEqualToSize(size, CGSizeZero) ? size : PHImageManagerMaximumSize) contentMode:PHImageContentModeAspectFill options:opt resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        if (callBack) {
            callBack(result);
        }
    }];
}
/**
 *获取一个组里边PHAsset
 */
-(void)getAllPhotosWithGroup:(JSPhotosGroup *)group andPhotos:(BOOL)isPhotos andFinished:(void(^)(id objc))callBack{
    NSMutableArray *array = [@[] mutableCopy];
    PHFetchResult <PHAsset *>* result = [self getallPhotoWithPHAssetCollection:group.assetCollection withPhotos:isPhotos];
    for (PHAsset *asset in result) {
//        [self getAssetsImageWithAsset:asset andFinish:^(id objc) {
            [array addObject:asset];
//        }];
    }
    if (callBack) {
        callBack(array);
    }
}
@end

