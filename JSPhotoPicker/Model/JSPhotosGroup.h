//
//  JSPhotosGroup.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
@interface JSPhotosGroup : NSObject
/**
 *  组名
 */
@property (nonatomic , copy) NSString *groupName;

/**
 *  组的真实名
 */
@property (nonatomic , copy) NSString *realGroupName;

/**
 *  缩略图
 */
@property (nonatomic , strong) UIImage *thumbImage;

/**
 *  组里面的图片个数
 */
@property (nonatomic , assign) NSInteger assetsCount;


@property (nonatomic , strong) PHAssetCollection *assetCollection;
@end
