//
//  ViewController.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)selectPhoto:(id)sender;


@end

