//
//  JSPhotoPickerAssetsCollectionViewCell.h
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/13.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

@protocol JSPhotoPickerAssetsCollectionViewCellDelegate <NSObject>
-(void)js_photoPickerCollectionImageViewTapAction:(UIImageView *)imageView andPHAsset:(PHAsset *)asset;
-(void)js_photoPickerCollectionSelectedAction:(UIButton *)selectButton andPHAsset:(PHAsset *)asset;

@end

@interface JSPhotoPickerAssetsCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)PHAsset *asset;
@property(nonatomic, strong)UIImageView *imageView;
@property(nonatomic,strong)UIButton *selectButton;
@property(nonatomic, weak)id <JSPhotoPickerAssetsCollectionViewCellDelegate>delegate;

-(void)updateCellWithPHAsset:(PHAsset *)asset andSelected:(BOOL)selected;
@end
