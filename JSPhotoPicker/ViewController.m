//
//  ViewController.m
//  JSPhotoPicker
//
//  Created by JasonLee on 2018/8/10.
//  Copyright © 2018年 JasonLee. All rights reserved.
//

#import "ViewController.h"

#import "JSPhotoPickerDatas.h"
#import "JSPhotoPickerViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)selectPhoto:(id)sender {
    JSPhotoPickerViewController *pick = [JSPhotoPickerViewController new];
    pick.maxCount = 6;
    pick.status = PickerViewShowStatusCameraRoll;
    [pick showPickerViewController:self];
    pick.callBack = ^(NSMutableArray<PHAsset *> *assetArray, NSMutableArray<UIImage *> *imageArray) {
        for (int i = 0; i< 6; i++) {
            UIImageView *imageView = [self.view viewWithTag:i+1];
            imageView.image = imageArray[i];
        }
    };
//    pick.callBack = ^(id obj) {
//        NSArray *array = (NSArray *)obj;
//        NSLog(@"选择的图片%@\n数量:%ld",array,(long)array.count);
//        for (int i = 0; i< 6; i++) {
//            UIImageView *imageView = [self.view viewWithTag:i+1];
//            imageView.image = array[i];
//        }
//    };
}
@end
